public class CarRace {

    public static void main(String[] args){

        Car ford = new Ford();
        Car opel = new Opel();
        Car renault = new Renault();

        doRace(ford);
        doRace(opel);
        doRace(renault);
    }

    public static void doRace(Car car){
        car.increaseSpeed();
        car.increaseSpeed();
        car.increaseSpeed();

        car.decreaseSpeed();
        car.decreaseSpeed();

        System.out.println(car.getSpeed());
    }
}