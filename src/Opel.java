public class Opel implements Car{

    private int speed;

    public Opel(){
        this.speed = 0;
    }

    @Override
    public int getSpeed() {
        return this.speed;
    }

    @Override
    public void increaseSpeed() {
        this.speed += 27;
    }

    @Override
    public void decreaseSpeed() {
        this.speed -= 25;
    }
}
